#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 20:36:53 2020

@author: armin
"""

#%% import libraries
import numpy as np
import matplotlib.pyplot as plt
import datetime
import time
import os

#%% define functions

"""
h contains the channel gains of the channels from all actually transmitting 
antennas to all users.
h_block1 contains the channel gains of all channels from block 1 to user 1.
These are needed by the ML detector.
Without loss of generality, it is assumed, that always the first antenna of
a block is sending.
"""
def create_channel_gains(N_u, N_t, sigma_fading):
    
    h = np.zeros((N_u,N_u), dtype=complex) # initialize used channel gain matrix
    h_block1 = np.zeros(N_t, dtype=complex) # initialize channel gains from first block to first user
    
    i = 0
    while i < N_u:
        j = 0
        while j < N_u:
            h[i,j] = np.random.normal(0, sigma_fading/np.sqrt(2)) + 1j * np.random.normal(0, sigma_fading/np.sqrt(2))
            j += 1
        i += 1
        
    i = 1
    while i < N_t:
        h_block1[i] = np.random.normal(0, sigma_fading/np.sqrt(2)) + 1j * np.random.normal(0, sigma_fading/np.sqrt(2))
        i += 1
    h_block1[0] = h[0,0] # the first antenna of block one is the sending antenna
    
    return h, h_block1

#------------------------------------------------------------------------------

"""
The functions returns an array with one sending signal for each user.
"""
def create_transmit_signals(N_u, M):
    
    x = np.zeros(N_u, dtype=complex)
    x_number = np.zeros(N_u, dtype=int)
    
    i = 0
    while i < N_u:
        x_number[i] = np.random.randint(M)
        phi = (2*np.pi/M) * x_number[i]
        x[i] = np.cos(phi) + 1j * np.sin(phi)
        i += 1
        
    return x, x_number[0]

#------------------------------------------------------------------------------

"""
H_eff is the "effective channel-modulation matrix", like it is used in the 
paper.
"""    
def calculate_effective_modulation_matrix(h, x, N_u):
    
    H_eff = np.zeros((N_u,N_u), dtype=complex)
    
    i = 0
    while i < N_u:
        j = 0
        while j < N_u:
            H_eff[j,i] = h[j,i] * x[i]
            j += 1
        i += 1
    
    return H_eff

#------------------------------------------------------------------------------
    
"""
The function calculates the preprocessed data by calculating the precoder.
"""
def calculate_preprocessed_data(H_eff, N_u):
    
    # calculating the precoder
    H_eff_inv = np.linalg.inv(H_eff)
    H_eff_diag = np.diag(H_eff)
    H_eff_diag = H_eff_diag.reshape(N_u,1)
    P = H_eff_inv.dot(H_eff_diag)
    
    #calculating the preprocessed data
    H_req = H_eff.dot(P)
    
    return H_req

#------------------------------------------------------------------------------

"""
Calculates the receive signals by adding random noise.
"""    
def calculate_receive_signals(H_req, sigma_noise, E_sqrt, beta, N_u):
    
    # creating noise
    n = np.ones(N_u, dtype=complex)
    i = 0
    while i < N_u:
        n[i] = np.random.normal(0, sigma_noise/np.sqrt(2)) + 1j * np.random.normal(0, sigma_noise/np.sqrt(2))
        i += 1
    n = n.reshape(N_u,1)
    
    # calculating the received symbol
    Y = H_req.dot(E_sqrt * beta) + n
    
    return Y

#------------------------------------------------------------------------------
    
def ml_decoder(Y, x_number, h_block1, M, N_t, E_sqrt, beta):
    
    differences = np.ones((M,N_t), dtype=float)
    
    min_diff = 10
    i = 0
    while i < M:
        phi_possible = (2*np.pi/M) * i
        x_possible = np.cos(phi_possible) + 1j * np.sin(phi_possible)
        j = 0
        while j < N_t:
            difference = np.absolute(Y[0] - E_sqrt * beta * h_block1[j] * x_possible)
            differences[i,j] = difference
            if difference < min_diff:
                min_diff = difference
                x_number_min = i
                t_min = j                
            j += 1
        i += 1
        
        if x_number_min == x_number and t_min == 0:
            transmission_success = True
        else:
            transmission_success = False
        
    return differences, transmission_success

#%% main
    
name = 'MU-SM'
date_object = datetime.datetime.utcnow() # utc time is used
   
N_u = 4 # number of users
N_t = 2 # number of antennas per block
M = 2 # used modulation scheme (M-PSK)
modulation_sceme = '%i-PSK' % M # adjust output in csv file according to modulation sceme

sigma_fading = 1
beta = 1

# parameter for fast simulation
SNR_dB_list = np.array([0, 10, 20, 40])
errors_required_list = np.array([1000, 1000, 1000, 10])

# parameter for elaborate simulation
#SNR_dB_list = np.array([0, 2.5, 5, 7.5, 10, 15, 20, 25, 30, 40, 50])
#errors_required_list = np.array([100000, 100000, 10000, 10000, 1000, 1000, 1000, 100, 100, 10, 10])

#------- start of calculation ------------------------------------------------#
time_start = time.time()

iterator = np.arange(len(SNR_dB_list))
ASEP_list = np.ones(len(SNR_dB_list))

for index in iterator:
    
    time_start_round = time.time() # taking time for measuring
    
    SNR = 10**(SNR_dB_list[index]/10)
    errors_required = errors_required_list[index]
    
    sigma_noise = 1/np.sqrt(SNR)
    sigma_fading = 1 # Rayleigh fading is normalized
    E_m = 1 # energy is set to one for the beginnig
    E_sqrt = np.sqrt(E_m) # in the formulas, most of the time, the square root of the energy is needed

    symbol_error = 0
    transmission_count = 0
    
    while symbol_error < errors_required:
        
        h, h_block1 = create_channel_gains(N_u, N_t, sigma_fading)
        x, x_number = create_transmit_signals(N_u, M)
        H_eff = calculate_effective_modulation_matrix(h, x, N_u)
        H_req = calculate_preprocessed_data(H_eff, N_u)
        Y = calculate_receive_signals(H_req, sigma_noise, E_sqrt, beta, N_u)
        differences, transmission_success = ml_decoder(Y, x_number, h_block1, M, N_t, E_sqrt, beta)
        
        if transmission_success == False:
            symbol_error += 1
        transmission_count += 1
        
    ASEP_list[index] = symbol_error / transmission_count
    
    time_end_round = time.time()
    time_round = time_end_round - time_start_round
    
    #print('SNR ' + str(SNR_dB_list[index]) + ' dB: ' + str(transmission_count) + ' transmissions, ' + str(time_round) + ' seconds needed.')
    print('SNR %d dB: %d transmissions, %.2f seconds needed.' % (SNR_dB_list[index], transmission_count, time_round))
    
print(ASEP_list)
time_end = time.time()
time_overall = time_end - time_start
print('Overall time needed: %.2f seconds.' % time_overall)

#%% show simple diagram

fig, ax = plt.subplots()

ax.plot(SNR_dB_list, ASEP_list)
ax.set_yscale('log')
ax.set_xlabel('signal to noise ratio')
ax.set_ylabel('average symbol error rate')
ax.set_title('ASEP over SNR')
ax.grid(True)

#%% save csv file

"""
Attention: When running this on a different machine, the used paths wont't 
work. In this case, the path has to be generated with 
os.path.dirname(os.path.abspath(__file__))
Because in Spyder, this works only when executing with F5, it is not 
implemented.
"""
date = date_object.strftime('%Y-%m-%dT%H:%MZ') # depiction of date and time after ISO 8601 without seconds
path = os.path.dirname(os.path.abspath(__file__))
file_name = '/home/armin/Dokumente/Oberseminar/git/results/PSK-QAM/' + name + '_' + date + '.csv'

parameter_string_1  = 'Number of users N_t = \t\t\t%i\n' % N_u
parameter_string_1a = 'Number of antennas (per user) N_u = \t\t%i\n' % N_t
parameter_string_2  = 'Simulated SNRs in dB: \t\t\t' + str(SNR_dB_list) + '\n'
parameter_string_3  = 'Required errors for the respective SNRs: \t' + str(errors_required_list) + '\n'
parameter_string_4  = 'Modulation sceme: \t\t\t\t%s' % modulation_sceme + '\n'
parameter_string_5  = 'Name of simulation: \t\t\t\t' + name + '\n'
date_string         = 'Date and Time: \t\t\t\t' + date_object.strftime('%Y-%m-%dT%H:%M:%SZ') + '\n'
time_string         = 'Overall time needed: \t\t\t\t%f\n' % time_overall
header =  parameter_string_5 + parameter_string_1  + parameter_string_1a + parameter_string_2 + parameter_string_3 + parameter_string_4 + date_string + time_string + '\n'

np.savetxt(file_name, np.c_[SNR_dB_list, ASEP_list], header=header, delimiter=',')

#%% test
sigma_noise = 1/np.sqrt(10**(40/10))
E_sqrt = 1

h, h_block1 = create_channel_gains(N_u, N_t, sigma_fading)
#print(h_block1)
x, x_number = create_transmit_signals(N_u, M)
print(x, x_number)
H_eff = calculate_effective_modulation_matrix(h, x, N_u)
#print(H_eff)
H_req = calculate_preprocessed_data(H_eff, N_u)
#print(H_req)
Y = calculate_receive_signals(H_req, sigma_noise, E_sqrt, beta, N_u)
print(Y)
differences, transmission_success = ml_decoder(Y, x_number, h_block1, M, N_t, E_sqrt, beta)

print(differences)
print(transmission_success)
