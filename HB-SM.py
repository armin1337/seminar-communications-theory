#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 14:28:07 2020

@author: armin
"""

#%% import libraries
import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
import datetime
import time
#import os

#%% function definitions

def create_channel(N_T, N_R, N_A, L, sigma_fading):
    
    a_T_matrix = np.zeros((N_T, L, N_A), dtype=complex)
    H = np.zeros((N_R, N_T, N_A), dtype=complex)    
    
    for j in range(N_A):
        
        sigma = np.matlib.zeros((N_R, N_T), dtype=complex) # sigma refers to the sigma sign
    
        
        i = 0
        while i < L:
            alpha = np.random.normal(0, sigma_fading/np.sqrt(2)) + 1j * np.random.normal(0, sigma_fading/np.sqrt(2))
            
            theta = np.random.uniform(0, 2*np.pi)
            a_R = create_array_response_vector(N_R, theta)
            
            phi = np.random.uniform(0, 2*np.pi)       
            a_T = create_array_response_vector(N_T, phi) 
            a_T_matrix[:,i,j] = a_T.reshape(N_T)
            
            sigma += alpha * a_R @ a_T.conj().transpose() # @ means matrix multiplication
            i += 1
            
        H[:,:,j] = np.sqrt(N_T * N_R / L) * sigma
    
    return H, a_T_matrix

def create_array_response_vector(N, theta):
        
    a = np.ones(N, dtype=complex)
    i = np.arange(0,N)
    
    a = np.exp(1j * np.pi * i * np.sin(theta))
    a = np.sqrt(1/N) * a   
    return a.reshape(N,1)

def create_transmit_signals(K, N_A, M):
    s = np.zeros(K, dtype=complex)
    s_number = np.zeros(K, dtype=int) # number of modulated signal
    aa_number = np.zeros(K, dtype=int) # number of used AA
    
    for i in range(K):
        s_number[i] = np.random.randint(M) # choose modulated signal
        aa_number[i] = np.random.randint(N_A) # choose used AA
        
        phi = (2*np.pi/M) * s_number[i] + 2*np.pi/(2*M)
        s[i] = np.sqrt(2) * (np.cos(phi) + 1j * np.sin(phi))
        
    return s, s_number, aa_number

def calculate_beamformer_vector_complete(H, a_T_matrix, aa_number):
    K = 1
    k = 0
    # iteration over K is not yet implemented
    
    F_matrix = np.ones((K, N_A, N_T), dtype=complex)
    
    for j in range(N_A):       
        f_abs = np.zeros(1, dtype=float)
        f_abs_max = np.zeros(1, dtype=float)
        
        for i in range(L):
            f_abs = np.linalg.norm(H[:,:,j] @ a_T_matrix[:,i,j])
            if f_abs > f_abs_max:
                f_abs_max = f_abs
                F_matrix[k,j,:] = a_T_matrix[:,i,j]
    
    f = F_matrix[k,aa_number,:]
    
    return F_matrix, f.reshape(N_T,1)

def calculate_H_i(H, F_matrix):
    K = 1
    k = 0
    # iteration over K is not yet implemented
    H_i = np.ones((N_R, N_A), dtype=complex)
    
    for j in range(N_A): # from 0 to N_A
        H_i[:,j] = H[:,:,j] @ F_matrix[k,j,:]
            
    return H_i

def calculate_W(H_i):
    # the direct calculation of the pseudo inverse should be circumvented!
    H_pseudo_inverse = np.linalg.pinv(np.asmatrix(H_i), rcond=1e-15, hermitian=False)
    W = H_pseudo_inverse.H
    return np.array(W)

def calculate_effective_channel_matrix(H, f, W, aa_number):
    K = 1
    k = 0
    # iteration over K is not yet implemented

    H_eff = W[:,aa_number[k]].conj().transpose() * H[:,:,aa_number[k]] @ f
    return H_eff

def calculate_beta(H_eff, K):
    beta = 0.8966 # not yet implemented
    return beta

def calculate_precoder(beta, H_eff):
    # the direct calculation of the pseudo inverse should be circumvented!
    P = beta * np.linalg.pinv(np.asmatrix(H_eff))
    return np.array(P)

def PSK_SM_mapper(s_number, aa_number, M):
    
    binary_string_PSK = bin(s_number ^ (s_number >> 1)) # algorithm from Wikipedia (implements Grey code)
    binary_string_SM = bin(aa_number)
    length_string_PSK = len(binary_string_PSK)
    length_string_SM = len(binary_string_SM)
    
    length_array = int(np.log2(M)) + int(np.log2(N_A))
    bit_array = np.zeros(length_array, 'int')
        
    for i in range(length_string_PSK - 2):
        bit_array[length_array - i - 1] = int(binary_string_PSK[length_string_PSK - i - 1])
    for i in range(length_string_SM - 2):
        bit_array[length_array - 1 - i - int(np.log2(M))] = int(binary_string_SM[length_string_SM - i - 1])

    return bit_array

def calculate_receive_signal(W, H, f, P, s, aa_number, sigma_noise):
    k = 0
    # indexing of the respective user or iteration over all users not yet implemented
    
    n = np.ones((N_R,1), dtype=complex)
    for i in range(N_R):
        n[i] = np.random.normal(0, sigma_noise/np.sqrt(2)) + 1j * np.random.normal(0, sigma_noise/np.sqrt(2)) 
    r = H[:,:,aa_number[k]] @ f * P * s + n
    
    return r

def ml_decoder(r, s_number, aa_number, N_A, W, M, beta):

    aa_number_decoded = 0
    r_number_decoded = 0
    bit_errors = 0
    
    i = np.arange(0,M)
    phi = (2*np.pi/M) * i + 2 * np.pi / (2 * M)
    s = np.sqrt(2) * (np.cos(phi) + 1j * np.sin(phi))
    r_matrix = W.conj().transpose() @ r @ np.ones(M).reshape(1,M)
    s_matrix = np.ones(N_A).reshape(N_A,1) @ s.reshape(1,M)
    differences = abs(r_matrix - s_matrix)
    r_number_decoded = differences.argmin() % M
    aa_number_decoded = differences.argmin() % N_A
    
    if r_number_decoded == s_number and aa_number_decoded == aa_number:
        transmission_success = True
    else:
        transmission_success = False
        bit_array_tx = PSK_SM_mapper(s_number, aa_number, M)
        bit_array_rx = PSK_SM_mapper(r_number_decoded, aa_number_decoded, M)
        hamming_distance = np.bitwise_xor(bit_array_tx, bit_array_rx).sum()
        bit_errors = hamming_distance
    
    return bit_errors, transmission_success

#%% main
    
name = 'HBF-SM'
date_object = datetime.datetime.utcnow() # utc time is used
   
L = 10 # number of paths
K = 1 # number of users
N_A = 2 # number of antenna arrays (AAs)
N_T = 8 # number of antennas per AA
N_R = 1 # number of antennas per user
M = 4 # used modulation scheme (M-PSK)
modulation_sceme = '%i-PSK' % M # adjust output in csv file according to modulation sceme

sigma_fading = 1

# parameter for fast simulation
SNR_dB_list = np.array([0, 5, 10, 15])
errors_required_list = np.array([100, 100, 100, 100])

# parameter for elaborate simulation
#SNR_dB_list = np.array([0, 2.5, 5, 7.5, 10, 15, 20, 25, 30, 40, 50])
#errors_required_list = np.array([100000, 100000, 10000, 10000, 1000, 1000, 1000, 100, 100, 10, 10])

#------- start of calculation ------------------------------------------------#
time_start = time.time()

iterator = np.arange(len(SNR_dB_list))
BER_list = np.ones(len(SNR_dB_list))
print('Start calculation.')

for index in iterator:
    
    time_start_round = time.time() # taking time for measuring
    
    SNR = 10**(SNR_dB_list[index]/10)
    errors_required = errors_required_list[index]
    
    sigma_noise = 1/np.sqrt(SNR)
    sigma_fading = 1 # Rayleigh fading is normalized
    E_m = 1 # energy is set to one for the beginnig
    E_sqrt = np.sqrt(E_m) # in the formulas, most of the time, the square root of the energy is needed

    symbol_error = 0
    bit_error_count = 0
    transmission_count = 0
    
    while symbol_error < errors_required:
        
        k = 0
        
        H, a_T_matrix = create_channel(N_T, N_R, N_A, L, sigma_fading)
        s, s_number, aa_number = create_transmit_signals(K, N_A, M)
        
        F_matrix, f = calculate_beamformer_vector_complete(H, a_T_matrix, aa_number)
        H_i = calculate_H_i(H, F_matrix)
        W = calculate_W(H_i)
        H_eff = calculate_effective_channel_matrix(H, f, W, aa_number)
        beta = calculate_beta(H_eff, K)
        P = calculate_precoder(beta, H_eff)
        r = calculate_receive_signal(W, H, f, P, s, aa_number, sigma_noise)
        bit_errors, transmission_success = ml_decoder(r, s_number[k], aa_number[k], N_A, W, M, beta)
        
        if transmission_success == False:
            symbol_error += 1
        bit_error_count += bit_errors
        transmission_count += 1
        
        
        if transmission_count > 100000:
            break
        
    BER_list[index] = bit_error_count / (transmission_count * np.log2(M))
    
    time_end_round = time.time()
    time_round = time_end_round - time_start_round
    
    #print('SNR ' + str(SNR_dB_list[index]) + ' dB: ' + str(transmission_count) + ' transmissions, ' + str(time_round) + ' seconds needed.')
    print('SNR %d dB: %d transmissions, %.2f seconds needed.' % (SNR_dB_list[index], transmission_count, time_round))
    
print(BER_list)
time_end = time.time()
time_overall = time_end - time_start
print('Overall time needed: %.2f seconds.' % time_overall)

#%% show simple diagram

fig, ax = plt.subplots()

ax.plot(SNR_dB_list, BER_list)
ax.set_yscale('log')
ax.set_xlabel('signal to noise ratio')
ax.set_ylabel('bit error rate')
ax.set_title('BER over SNR')
ax.grid(True)

#%% save simple plot

date = date_object.strftime('%Y-%m-%dT%H:%MZ') # depiction of date and time after ISO 8601 without seconds
name = 'HBF-SM_test'
file_name = '/home/armin/Dokumente/Oberseminar/git/results/HBF-SM/pdf/' + name + '_' + date + '.pdf'

fig.savefig(file_name, bbox_inches='tight')

#%% save csv file

"""
Attention: When running this on a different machine, the used paths wont't 
work. In this case, the path has to be generated with 
os.path.dirname(os.path.abspath(__file__))
Because in Spyder, this works only when executing with F5, it is not 
implemented.
"""
date = date_object.strftime('%Y-%m-%dT%H:%MZ') # depiction of date and time after ISO 8601 without seconds
#path = os.path.dirname(os.path.abspath(__file__))
file_name = '/home/armin/Dokumente/Oberseminar/git/results/HBF-SM/' + name + '_' + date + '.csv'

parameter_string_1  = 'Number of users K = \t\t\t%i\n' % K
parameter_string_1a = 'Number of AAs N_A = \t\t\t%i\n' % N_A
parameter_string_1b = 'Number of antennas per user N_R = \t\t\t%i\n' % N_R
parameter_string_1c = 'Number of antennas per AA N_T = \t\t%i\n' % N_T
parameter_string_2  = 'Simulated SNRs in dB: \t\t\t' + str(SNR_dB_list) + '\n'
parameter_string_3  = 'Required errors for the respective SNRs: \t' + str(errors_required_list) + '\n'
parameter_string_4  = 'Modulation sceme: \t\t\t\t%s' % modulation_sceme + '\n'
parameter_string_5  = 'Name of simulation: \t\t\t\t' + name + '\n'
date_string         = 'Date and Time: \t\t\t\t' + date_object.strftime('%Y-%m-%dT%H:%M:%SZ') + '\n'
time_string         = 'Overall time needed: \t\t\t\t%f\n' % time_overall
header =  parameter_string_5 + parameter_string_1  + parameter_string_1a + parameter_string_1b + parameter_string_1c + parameter_string_2 + parameter_string_3 + parameter_string_4 + date_string + time_string + '\n'

np.savetxt(file_name, np.c_[SNR_dB_list, ASEP_list], header=header, delimiter=',')
#%% test
    
L = 10
N_T =  8
N_R = 1
M = 4
N_A = 2

sigma_fading = 1
SNR_dB = 40
SNR = 10 ** (SNR_dB/10)
sigma_noise = 1/np.sqrt(SNR)

K = 1
k = 0

H, a_T_matrix = create_channel(N_T, N_R, N_A, L, sigma_fading)
print('H')
print(H)
s, s_number, aa_number = create_transmit_signals(K, N_A, M)
print('s number: %d, aa number: %d' % (s_number, aa_number))

F_matrix, f = calculate_beamformer_vector_complete(H, a_T_matrix, aa_number)
print('f')
print(f)
H_i = calculate_H_i(H, F_matrix)
print('H_i')
print(H_i)
W = calculate_W(H_i)
print('W')
print(W)
H_eff = calculate_effective_channel_matrix(H, f, W, aa_number)
print('H_eff')
print(H_eff)
beta = calculate_beta(H_eff, K)
P = calculate_precoder(beta, H_eff)
print('P')
print(P)
r = calculate_receive_signal(W, H, f, P, s, aa_number, sigma_noise)
print('r')
print(r)
bit_errors, transmission_success = ml_decoder(r, s_number[k], aa_number[k], N_A, W, M, beta)
print(transmission_success)
print(bit_errors)
